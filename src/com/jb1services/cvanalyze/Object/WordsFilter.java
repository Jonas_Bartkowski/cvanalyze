/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jb1services.cvanalyze.Object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jonas
 */
public class WordsFilter 
{
    public static final WordsFilter STANDARD_DE_FILTER = new WordsFilter("ihre","werden","gehören","(m/w)","eines","Wort","können","ihren","dann","+","&",".","+49","ihres","dieser","vor","ihrem","ihrer","unternehmen","-","nach","Jahre","Aufgaben","bringen","Gmbh","einen","ihnen","/","bitte","senden","Stelle","Bereich","unser","unseres","bzw.","um","-","zum","sehr","unter","bzw","und","unseren","einer","durch","per","haben","das","ihr","sich","verfügen","","unsere","bei","über","dem","auch","zur","aus","freuen","bieten","ist","gute","wie","suchen","neue","you","We","on","be","Dich","Du","als","we","des","Sie","oder","du","sind","zu","unserer","usere","unserem","einem","to","wir","for","your","are","ein","auf","den","uns","Kunden","an","im","eine","as","or","sowie","a","and","of","with","our","in","the","und","die","in","für","mit","der","am","von");
    private boolean equalsIgnoreCase = true;
               
    private List<String> filteredWords = new ArrayList();

    public WordsFilter(String... wordsToFilter)
    {
        this.filteredWords = Arrays.asList(wordsToFilter);
    }
    
    public WordsFilter(List<String> filteredWords)
    {
        this.filteredWords = filteredWords;
    }
    
    public List<String> getFilteredWords() 
    {
        return filteredWords;
    }

    public void setFilteredWords(List<String> filteredWords) 
    {
        this.filteredWords = filteredWords;
    }
    
    public static List<WordsFilter> getWordsFiltersAsList(WordsFilter... wf)
    {
        return Arrays.asList(wf);
    }
    
    public boolean isWordCaught(String word)
    {
        if(!equalsIgnoreCase)
        return filteredWords.contains(word);
        else
        {
            if (this.filteredWords.stream().anyMatch((f) -> (f.equalsIgnoreCase(word)))) {
                return true;
            }
            return false;
        }
    }
}
