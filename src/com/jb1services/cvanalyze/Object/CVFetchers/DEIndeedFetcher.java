/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jb1services.cvanalyze.Object.CVFetchers;

import com.jb1services.cvanalyze.Object.CVFetcher;
import com.jb1services.cvanalyze.Object.URLContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jonas
 */
public class DEIndeedFetcher extends CVFetcher {
    
    public static final boolean DEBUG = true;
    
    public static final String SUB_JOB_BASE_URL = "https://de.indeed.com/Zeige-Job?jk=";
    public static final String SUB_JOB_BEGIN_STRING = "{jk:'";
    public static final String SUB_JOB_END_STRING = "',";
    public static final String JOB_CONTENT_BEGIN_STRING = "<span id=\"job_summary\" class=\"summary\">";
    public static final String JOB_CONTENT_END_STRING = "<div class=\"result-link-bar-container";
    
    public DEIndeedFetcher() {
        //super(new URLContext("https://de.indeed.com/{JOB}-Jobs-in-{PLACE}","{JOB}","{PLACE}"));
        super(new URLContext("https://de.indeed.com/Jobs?q={JOB}&l={PLACE}&start={NUMRES}","{JOB}","{PLACE}","{NUMRES}"));
    }

    @Override
    public List<String> getResults(int maxNumResults, String... keywordReplacements) 
    {        
        if (keywordReplacements.length != super.URLContext.getKeywords().length)
        {
            String keywordString = ""; String[] kws = super.URLContext.getKeywords();
            for (int i = 0; i < kws.length; i++)
            {
                String kw = kws[i];
                if (i == kws.length-1)
                {
                    keywordString += kw;
                }
                else if (i == kws.length-2)
                {
                    keywordString += "and ";
                }
                else
                {
                    keywordString += kw + ", ";
                }
            }
            throw new IllegalArgumentException("This Fetcher requires exactly "+kws.length+" keyword replacements for "+keywordString+ ".");
        }
        
        
        keywordReplacements = formatKeywords(keywordReplacements);
        sout("Opening URL '"+this.URLContext.completeURL(keywordReplacements)+"'");
        try 
        {
            URL de_indeed = new URL(this.URLContext.completeURL(keywordReplacements));
            BufferedReader in = new BufferedReader(new InputStreamReader(de_indeed.openStream()));
            
        
            String line = null; List<String> results = new ArrayList(); int res_count = 0;
            while ((line = in.readLine()) != null && res_count < maxNumResults)
            {
                //sout(line);
                if (line.contains(SUB_JOB_BEGIN_STRING))
                {
                    sout("Marker found! line = "+line);                   
                    String subURL = SUB_JOB_BASE_URL+line.substring(line.indexOf(SUB_JOB_BEGIN_STRING)+SUB_JOB_BEGIN_STRING.length(), line.indexOf(SUB_JOB_END_STRING));
                    sout("Opening '" + subURL + "'");
                    URL sub_job = new URL(subURL);
                    BufferedReader sub_in = new BufferedReader(new InputStreamReader(sub_job.openStream()));
                    
                    line = null; String result = "";
                    while ((line = sub_in.readLine()) != null)
                    {
                        result += line;
                        //sout("line added!");
                    }
                    result = result.substring(result.indexOf(JOB_CONTENT_BEGIN_STRING)+JOB_CONTENT_BEGIN_STRING.length(), result.indexOf(JOB_CONTENT_END_STRING));
                    results.add(result); res_count++;
                }
                else
                {
                    //sout("No marker found...");
                }
                
                try {Thread.sleep(10);} catch (InterruptedException ex) {
                    Logger.getLogger(DEIndeedFetcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            sout("Finished reading & processing results!");
            int stillNeeded = maxNumResults - results.size();
            if (stillNeeded > 0)
            {
                sout("However we still need "+stillNeeded+" more results...\n");
                keywordReplacements[2] = ""+(Integer.valueOf(keywordReplacements[2])+(stillNeeded > 10 ? 10 : stillNeeded));
                results.addAll((getResults(maxNumResults-(stillNeeded > 10 ? 10 : stillNeeded), keywordReplacements)));
            }
            
            String[] results_a = new String[results.size()];
            return results;
        } 
        catch (MalformedURLException ex) {
            Logger.getLogger(DEIndeedFetcher.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        } 
        catch (IOException ex) {
            Logger.getLogger(DEIndeedFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void sout(String s)
    {
        if(DEBUG)System.out.println(s);
    }
    
    
}
