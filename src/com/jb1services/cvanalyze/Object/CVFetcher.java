/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jb1services.cvanalyze.Object;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jonas
 */
public abstract class CVFetcher 
{
    //public static final List<String> TRIVIAL_WORDS = Arrays.asList(new String[]{"unsere","bei","über","dem","auch","zur","aus","freuen","bieten","ist","gute","wie","suchen","neue","you","We","on","be","Dich","Du","als","we","des","Sie","oder","du","sind","zu","unserer","usere","unserem","einem","to","wir","for","your","are","ein","auf","den","uns","Kunden","an","im","eine","as","or","sowie","a","and","of","with","our","in","the","und","die","in","für","mit","der","am","von"});  
    public static final String[] HTML_TAGS = new String[]{"<p>","<li>","<br>","<br>","<ul>","<b>","<span>"};
    
    
    protected URLContext URLContext;
    //protected List<WordsFilter> wordsFilters = WordsFilter.getWordsFiltersAsList(WordsFilter.STANDARD_FILTER);
    
    public CVFetcher(URLContext urlcontext)
    {
        this.URLContext = urlcontext;
    }
    
    public abstract List<String> getResults(int maxNumResults, String... keywordReplacements);
    
    private String cleanHTMLTags(String input)
    {
        for (String tag: HTML_TAGS)
        {
            input = input.replace(tag, "");
            input = input.replace(tag.replaceFirst("<", "</"), "");
        }
        return input;
    }
    
    public final void saveResultsToFiles(String folder, int maxNumResults, String... keywordReplacements)
    {
        List<String> results = this.getResults(maxNumResults, keywordReplacements);
        int counter = 1;
        for (String result: results)
        {
            counter++;
            try 
            {
                System.out.println(folder);
                Files.write(Paths.get(folder+(folder.endsWith("\\") ? "" : "\\")+"\\"+"result "+counter+".html"), result.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(CVFetcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }   
    }
    
    public final void printRankingOfWords(List<WordsFilter> wordsFilters, int minimumAbundanceToCount,int maxNumResults, String... keywordReplacements)
    {
        List<String> results = this.getResults(maxNumResults, keywordReplacements);
        System.out.println("\nPrinting out results for control purposes:");
        int count = 0;
        for(String result: results)
        {
            result = cleanHTMLTags(result);
            System.out.println("result "+count+": "+result);
            count++;
        }
        System.out.println("done.");
        System.out.println("");
        System.out.println("counting words...");
        //counting words
        List<String> words = new ArrayList();
        List<Integer> wordCounts = new ArrayList();
        for(String result: results)
        {
            String[] wordsOfResult = result.split(" ");
            for (String wordOfResult: wordsOfResult)
            {
                if (wordsFilters == null || !isWordCaughtInFilters(wordOfResult, wordsFilters))
                {               
                    int index = words.indexOf(wordOfResult);
                    if (index != -1)
                    {
                        wordCounts.set(index, wordCounts.get(index)+1);
                    }  
                    else
                    {
                        words.add(wordOfResult);
                        wordCounts.add(1);
                    }
                }
            }
        }
        System.out.println("done.");
        System.out.println("--\n");
        String string = ""; for (String rp: keywordReplacements){string+=rp+" - ";}
        System.out.println("Verarbeitung von query\n"+string+"\nabgeschlossen!");
        
        //sorting & printing out ranking
        int counter = 0;
        while (!words.isEmpty())
        {
            int max = 0; int index = -1;              
            for(int i = 0; i < words.size(); i++)
            {    
                if (wordCounts.get(i) > max){index = i; max = wordCounts.get(i);}
                
            }
            
            if (!(max < minimumAbundanceToCount))
            System.out.println("word #"+counter+": '"+words.get(index)+"' with "+max+" occurences!");
            words.remove(index); wordCounts.remove(index);
            counter++;
        }
    }
    
    public final void printRankingOfWords(int minimumAbundanceToCount,int maxNumResults, String... keywordReplacements)
    {
        printRankingOfWords(null, minimumAbundanceToCount, maxNumResults, keywordReplacements);
    }
    
    private boolean isWordCaughtInFilters(String word, List<WordsFilter> filters)
    {
        for (WordsFilter wf: filters)
        {
            if (wf.isWordCaught(word))
                return true;
        }
        return false;
    }
    
    protected String[] formatKeywords(String[] unformated)
    {
        String[] formated = new String[unformated.length];
        int i = 0;
        for (String unf: unformated)
        {
            formated[i] = formatKeyword(unf);
            i++;
        }
        return formated;
    }
    
    protected String formatKeyword(String unformated)
    {
        unformated = unformated.replace(" ", "%20");
        unformated = unformated.replace("ü", "%C3%BC");
        unformated = unformated.replace("ä", "%C3%A4");
        unformated = unformated.replace("ö", "%C3%B6");
        unformated = unformated.replace("ß", "%C3%9F");
        return unformated;
    }
}
