/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jb1services.cvanalyze.Object;

/**
 *
 * @author Jonas
 */
public class URLContext 
{
    private String URLContext;
    private String[] keywords;
    
    public URLContext(String URLContext, String... keywords)
    {
        this.URLContext = URLContext;
        for (String kw: keywords)
        {
            if (!URLContext.contains(kw)) 
            {
                throw new IllegalArgumentException("All keywords need to be a part of the URLContext!");
            }
        }
        this.keywords = keywords;
    }
    
    public String[] getKeywords()
    {
        return keywords;
    }
    
    public String completeURL(String[] replacersForKeywords)
    {
        String URL = URLContext;
        if (keywords == null || replacersForKeywords.length > keywords.length)
        {
            throw new IllegalStateException("Can't call completeURL(String[]) with non-exististing or less than necessary pre-defined keywords.");
        }
        
        for (int i = 0; i < keywords.length; i++)
        {
            URL = URL.replace(keywords[i], replacersForKeywords[i]);
        }
        
        for (int i = 0; i < keywords.length - replacersForKeywords.length; i++)
        {
            URL = URL.replace(keywords[i+replacersForKeywords.length], "");
        }
        return URL;
    }
    
    public String completeURL(String[] keywords, String[] replacersForKeywords)
    {
        return (new URLContext(this.URLContext, keywords)).completeURL(replacersForKeywords);
    }
}
