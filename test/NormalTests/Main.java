/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NormalTests;

import com.jb1services.cvanalyze.Object.CVFetchers.DEIndeedFetcher;
import com.jb1services.cvanalyze.Object.WordsFilter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jonas
 */
public class Main 
{
    public static void main(String[] args)
    {
        //testPrintResults();
        //testWriteFiles();
        //testOutBüro();
        testPrintRanking("Mechanik","Deutschland",100,2,WordsFilter.getWordsFiltersAsList(WordsFilter.STANDARD_DE_FILTER));
    }
    
    public static void testPrintResults()
    {
        List<String> results = (new DEIndeedFetcher()).getResults(5, "Java","Frankfurt");
        for (String result: results)
        {
            System.out.println(result);
            System.out.println("____________________________________________________");
        }
    }
    
    public static void testWriteFiles()
    {
        (new DEIndeedFetcher()).saveResultsToFiles("C:\\test",5, "Java","Frankfurt");
    }
    
    public static void testPrintRanking(String job, String location, int maxRes, int minAbundance, List<WordsFilter> wordsFilters)
    {
        (new DEIndeedFetcher()).printRankingOfWords(wordsFilters, minAbundance, maxRes, job, location,"0");
    }
    
    public static void testOutBüro()
    {
        try 
        {
            URL url = new URL("https://de.indeed.com/Jobs?q=b%C3%BCrokaufmann&l=Frankfurt");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String line = null;
            while ((line = in.readLine()) != null)
            {
                if (line.contains(DEIndeedFetcher.JOB_CONTENT_BEGIN_STRING))
                    System.out.println("HEUREKA!");
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
